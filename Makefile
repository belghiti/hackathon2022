BOOTSTRAP=1
PATTERN=example
COMPONENT=datacenter
SECRET_NAME="argocd-env"

.PHONY: default
default: show

%:
	make -f common/Makefile $*

install: 
	oc project default
	make -f common/Makefile deploy
	helm list

bootstrap: 
	mkdir -p ./certs
	echo "Create private key and certificate for frontend gateway"
	scripts/create-certificate.sh  
	echo "Create private key and certificate for client"
	scripts/create-client-certificate.sh
	echo "Create secret frontend-credential for TLS key, certificate and client certificates"
	oc create secret generic frontend-credential \
	--from-file=tls.key=certs/frontend.key \
	--from-file=tls.crt=certs/frontend.crt \
	--from-file=ca.crt=certs/acme.com.crt \
	-n istio-system
