#!/bin/sh
SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
curl -kv http://frontend.$DOMAIN
